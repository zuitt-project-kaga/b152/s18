const input = document.getElementById("input");

// input.innerHTML = "Hello"

let trainer = {
    name: "Masayhiro",
    age: 39,
    pockemon: ['Fushigidane', 'Myuutuu', 'Rapras', 'Bulbasur'],
    friens: { hoenn: ['May', 'Max'], kanto: ['Misty', 'Brock'] },
    talk: function() {
        console.log("Pikachu! I choose you!");
    }
}

console.log(trainer);
console.log('Result of dot notation :');
console.log(trainer.name)
console.log('Result of square bracket notation:')
console.log(trainer['pockemon']);
console.log('Result of talk method');
trainer.talk();

function Pockemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = level * 3;
    this.attack = level;

    this.tackle = function(target) {
        console.log(target) //object of charizard
        console.log(`${this.name} tackled ${target.name}`)
        console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`);

        target.health -= this.attack;
        if (target.health <= 0) {
            target.faint(target);
        }
    }

    this.faint = function(target) {
        console.log(`Pokemon ${target.name} fainted`)
    }

    console.log(this);
}

let fushigidane = new Pockemon('Fushigidane', 40);
let myuutuu = new Pockemon('Myuutuu', 70);
let rapras = new Pockemon('Rapras', 50);
let bulbasur = new Pockemon('Bulbasur', 30);

function battle(player1, player2) {
    while (true) {
        let attacker = Math.floor(Math.random() * 2);
        switch(attacker) {
            case 0:
                player1.tackle(player2);
            break;
            case 1:
            	player2.tackle(player1);
            break;
        }
        if (player1.health < 0 || player2.health < 0) {
        	break;
        }
    }
}

battle(fushigidane,bulbasur);