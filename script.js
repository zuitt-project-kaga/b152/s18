let input = document.getElementById('input');

// input.innerHTML = "hello world";

const studentGrades = [
    { studentId: 1, Q1: 89.3, Q2: 91.2, Q3: 93.3, Q4: 89.8 },
    { studentId: 2, Q1: 69.2, Q2: 71.3, Q3: 76.5, Q4: 81.9 },
    { studentId: 3, Q1: 95.7, Q2: 91.4, Q3: 90.7, Q4: 85.6 },
    { studentId: 4, Q1: 86.9, Q2: 74.5, Q3: 83.3, Q4: 86.1 },
    { studentId: 5, Q1: 70.9, Q2: 73.8, Q3: 80.2, Q4: 81.8 }
];

function averageCalculator(studentGrades){
	let average;
	let numOfStudent = studentGrades.length;
	for(let i = 0 ; i < numOfStudent ; i++){
		let sumPoint = 0;


		for(let j = 1 ; j <= Object.keys(studentGrades[i]).length - 1 ; j++){
			sumPoint += studentGrades[i][`Q${j}`];
		}
		// sumPoint = studentGrades[i].Q1 + studentGrades[i].Q2 + studentGrades[i].Q3 + studentGrades[i].Q4;

		average = (sumPoint/4).toFixed(1);
		studentGrades[i].individualAverage = parseFloat(average); 
		console.log(`Student ID ${studentGrades[i].studentId} average is ${average}`)
	}
}

averageCalculator(studentGrades);
console.log(studentGrades);




// ----------------------------------------------------





function Pokemon(name, lvl, hp) {
    this.name = name,
        this.level = lvl,
        this.health = hp * 2,
        this.attack = lvl,
        this.tackle = function(target) {
            console.log(target) //object of charizard

            //change the statement to "Pikachu tackled Charizard"
            console.log(`${this.name} tackled ${target.name}`)

            //chnage the statement to "Charizard's health is now reduced to #"
            console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`);
            target.health -= this.attack;

            if (target.health < 10) {
                target.faint();
            }
        },
        this.faint = function() {
            console.log(`Pokemon fainted`)
        }
}


let pikachu = new Pokemon("Pikachu", 5, 50)
let charizard = new Pokemon("Charizard", 8, 40)

let masachu = new Pokemon("Masachu", 5, 60);


/* Mini Activity

1. Create a new set of pokemon for battle (use Pokemon object constructor)
2. Solve the health of the pokemon that when tackled, the current value of target's health must reduced continuously as many times as the  tackle function is invoked.
3. If health of the target pokemon is now below 10, invoke the faint function. (Note: refactor faint function to display appropriate value to the statement)

*/

function battle(player1, player2) {
    while (true) {
        let attacker = Math.floor(Math.random() * 2);
        // console.log(attacker);
        switch(attacker) {
            case 0:
                player1.tackle(player2);
            break;
            case 1:
            	player2.tackle(player1);
            break;
        }
        if (player1.health < 10 || player2.health < 10) {
        	break;
        }
    }
}

battle(pikachu,masachu);



